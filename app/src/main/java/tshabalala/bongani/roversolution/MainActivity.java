package tshabalala.bongani.roversolution;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import tshabalala.bongani.roversolution.helper.Constants;

public class MainActivity extends AppCompatActivity {

    //Variables
    EditText editPositionRoverOne, editInstructionRoverOne;
    EditText editPositionRoverTwo, editInstructionRoverTwo;
    Button btnProcess;
    int foundRoverOne = 0;
    int xCoordinatesRoverOne = 0;
    int yCoordinatesRoverOne = 0;
    String directionRoverOne;
    int foundRoverTwo = 0;
    int xCoordinatesRoverTwo = 0;
    int yCoordinatesRoverTwo = 0;
    String directionRoverTwo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editPositionRoverOne = findViewById(R.id.editPositionRoverOne);
        editInstructionRoverOne = findViewById(R.id.editInstructionsRoverOne);

        editPositionRoverTwo = findViewById(R.id.editPositionRoverTwo);
        editInstructionRoverTwo = findViewById(R.id.editInstructionsRoverTwo);

        btnProcess = findViewById(R.id.buttonProcess);

        btnProcess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                processInput();
            }
        });

    }

    public void processInput(){

        String roverOnePosition = editPositionRoverOne.getText().toString().trim();
        String roverOneInstruction = editInstructionRoverOne.getText().toString().trim();

        String roverTwoPosition = editPositionRoverTwo.getText().toString().trim();
        String roverTwoInstruction = editInstructionRoverTwo.getText().toString().trim();



        if(!roverOnePosition.equals("")){

            int lengthRoverOne = roverOnePosition.length();
            if(lengthRoverOne == 3) {
                boolean varRoverOne = positionInput(roverOnePosition);
                if(varRoverOne) {
                    if(!roverOneInstruction.equals("")){

                        if(inputInstruction(roverOneInstruction)) {

                            if(!roverTwoPosition.equals("")){

                                int lengthRoverTwo = roverTwoPosition.length();
                                if(lengthRoverTwo == 3) {
                                    boolean varRoverTwo = positionInputRoverTwo(roverTwoPosition);
                                    if(varRoverTwo) {
                                        if(!roverTwoInstruction.equals("")){

                                            if(inputInstructionRoverTwo(roverTwoInstruction)) {

                                                MarsRover marsRover = new MarsRover();
                                                marsRover.setRoverPosition(xCoordinatesRoverOne,yCoordinatesRoverOne, setDirection(directionRoverOne));
                                                marsRover.roverCommand(roverOneInstruction);

                                                MarsRover marsRoverTwo = new MarsRover();
                                                marsRoverTwo.setRoverPosition(xCoordinatesRoverTwo,yCoordinatesRoverTwo, setDirectionRoverTwo(directionRoverTwo));
                                                marsRoverTwo.roverCommand(roverTwoInstruction);


                                                display("Rover 1: \t"+marsRover.printPosition() + "\nRover 2: \t"+ marsRoverTwo.printPosition());
                                            }

                                        }else {
                                            displayError("Instructions can't be empty!");
                                        }
                                    }else{

                                        fromWhichInput(foundRoverTwo);
                                    }
                                }else{
                                    displayError("Input characters should be equal to 3!");
                                }

                            }else{
                                displayError("Position can't be empty!");
                            }
                        }

                    }else {
                        displayError("Instructions can't be empty!");
                    }
                }else{

                    fromWhichInput(foundRoverOne);
                }
            }else{
                displayError("Input characters should be equal to 3!");
            }

        }else{
            displayError("Position can't be empty!");
        }
    }

    /**
     * Comparing string \
     * and return rover one direction
     */
    private int setDirection(String direction) {
        char dir = 'N';

        switch (direction) {
            case "N":

                dir = Constants.N;
                break;
            case "E":

                dir = Constants.E;
                break;
            case "S":

                dir = Constants.S;
                break;
            case "W":

                dir = Constants.W;
                break;
        }

        return dir;
    }

    /**
     * Comparing string \
     * and return rover two direction
     */
    private int setDirectionRoverTwo(String direction) {
        char dir = 'N';

        switch (direction) {
            case "N":

                dir = Constants.N;
                break;
            case "E":

                dir = Constants.E;
                break;
            case "S":

                dir = Constants.S;
                break;
            case "W":

                dir = Constants.W;
                break;
        }

        return dir;
    }

    // Clearing inputs
    public void clearInputs(){

        editInstructionRoverTwo.setText("");
        editPositionRoverTwo.setText("");
        editInstructionRoverOne.setText("");
        editPositionRoverOne.setText("");
    }


    /**
     * Verify rover 2 instruction commands
     */
    public boolean inputInstructionRoverTwo(String command){
        boolean valid = false;

        for(int idx = 0; idx < command.length(); idx++){
            valid = process(command.charAt(idx));
        }
        return valid;
    }

    /**
     * Verify rover 1 instruction commands
     */
    public boolean inputInstruction(String command){
        boolean valid = false;

        for(int idx = 0; idx < command.length(); idx++){
            valid = process(command.charAt(idx));
        }
        return valid;
    }

    /**
     * Verify which commands are allowed
     */
    private boolean process(Character command){
        if(command.equals('L') || command.equals('R') || command.equals('M')){
            return true;
        }else{
            displayError("Accepted commands {L, R, M} ");
            return false;
        }
    }

    /** Rover 2
     * Reading position characters
     * Getting inputs xCoordinates, yCoordinates and Direction
     */
    public boolean positionInputRoverTwo(String command){
        boolean valid = false;
        if(Character.isDigit(command.charAt(0))){
            xCoordinatesRoverTwo =  Integer.parseInt(String.valueOf(command.charAt(0)));
            if(Character.isDigit(command.charAt(1))){
                yCoordinatesRoverTwo = Integer.parseInt(String.valueOf(command.charAt(1)));

                if(Character.isAlphabetic(command.charAt(2))){
                    if(command.charAt(2) == 'N' || command.charAt(2) == 'E'|| command.charAt(2) == 'S' || command.charAt(2) == 'W'){
                        directionRoverTwo =  String.valueOf(command.charAt(2));
                        valid = true;
                    }else {

                        foundRoverTwo = 4;
                    }
                }else {

                    foundRoverTwo = 3;
                }
            }else {

                foundRoverTwo = 2;
            }
        }else {

            foundRoverTwo = 1;
        }

        return valid;
    }

    /** Rover 1
     * Reading position characters
     * Getting inputs xCoordinates, yCoordinates and Direction
     */
    public boolean positionInput(String command){
        boolean valid = false;
        if(Character.isDigit(command.charAt(0))){
            xCoordinatesRoverOne =  Integer.parseInt(String.valueOf(command.charAt(0)));
            if(Character.isDigit(command.charAt(1))){
                yCoordinatesRoverOne = Integer.parseInt(String.valueOf(command.charAt(1)));

                if(Character.isAlphabetic(command.charAt(2))){
                    if(command.charAt(2) == 'N' || command.charAt(2) == 'E'|| command.charAt(2) == 'S' || command.charAt(2) == 'W'){
                        directionRoverOne =  String.valueOf(command.charAt(2));
                        valid = true;
                    }else {

                        foundRoverOne = 4;
                    }
                }else {

                    foundRoverOne = 3;
                }
            }else {

                foundRoverOne = 2;
            }
        }else {

            foundRoverOne = 1;
        }

        return valid;
    }

    /**
     * Determine which error to display
     */
    private void fromWhichInput(int found){

            if(found == 1){
                displayError("Input character position 1 -  should be a Number!");
            }else if(found == 2){
                displayError("Input character position 2 -  should be a Number!");
            }else if(found == 3){

                displayError("Input character position 3 -  should be an Alphabet!");
            }else if(found == 4){
                displayError("Input character position 3 -  allowed letters{N, E, S, W}!");
            }

    }

    //Alert Dialog for displaying rover status
    private void display(String s) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Mars Rover");
        alertDialog.setMessage(s);
        alertDialog.setNegativeButton("Close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                clearInputs();
                dialogInterface.dismiss();
            }
        });

        alertDialog.show();
    }

    //Alert Dialog for displaying error
    private void displayError(String s) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle("Error");
        alertDialog.setMessage(s);
        alertDialog.setNegativeButton("Close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });

        alertDialog.show();
    }
}
