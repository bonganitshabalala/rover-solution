package tshabalala.bongani.roversolution.helper;

public class Constants {

    //Direction constants
    public static final int N = 1;
    public static final int E = 2;
    public static final int S = 3;
    public static final int W = 4;
}
