package tshabalala.bongani.roversolution;


import tshabalala.bongani.roversolution.helper.Constants;

class MarsRover {

    private int facing;
    private int xCoordinate;
    private int yCoordinate;

    MarsRover() {
    }

    void setRoverPosition(int xCoordinate, int yCoordinate, int facing){
        this.xCoordinate = xCoordinate;
        this.yCoordinate = yCoordinate;
        this.facing = facing;

    }

    //Displaying rover direction
    String printPosition(){
        char dir = 0;
        if (facing == 1){
            dir = 'N';
        }else if(facing == 2){
            dir = 'E';
        }else if(facing == 3){
            dir = 'S';
        }else if(facing == 4){
            dir = 'W';
        }

        return String.valueOf(xCoordinate) +
                " " +
                yCoordinate +
                " " +
                dir;

    }

    //Reading instructions characters
    void roverCommand(String command){
        for(int idx = 0; idx < command.length(); idx++){
            process(command.charAt(idx));
        }
    }

    //Checking instructions characters if they match rover movement
    private void process(Character command){
        if(command.equals('L')){
            turnLeft();
        }else  if(command.equals('R')){
            turnRight();
        }else  if(command.equals('M')){
            move();
        }
    }

    //Determining which direction the rover is facing
    private void move()
    {
        if (facing == Constants.N){
            yCoordinate++;
        }else if(facing == Constants.E){
            xCoordinate++;
        }else if(facing == Constants.S){
           yCoordinate--;
        }else if(facing == Constants.W){
            xCoordinate--;
        }

    }

    private void turnLeft(){

        facing = (facing -1) < Constants.N ? Constants.W : facing - 1;
    }

    private void turnRight(){
       facing = (facing +1) > Constants.W ? Constants.N : facing + 1;
    }


}
